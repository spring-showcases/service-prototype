package com.gitlab.spring.showcases.service.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class SpringServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringServiceApplication.class, args).registerShutdownHook();
    }

}
