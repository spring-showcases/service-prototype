package com.gitlab.spring.showcases.service.spring;

import com.gitlab.spring.showcases.service.core.product.Product;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@RestController
@RequestMapping("/showcase/product")
@Slf4j
public class SpringProductResourceB {

    @RequestMapping(path = "/viewB", method = {RequestMethod.GET},
            produces = {APPLICATION_XML_VALUE, APPLICATION_JSON_VALUE})
    @ResponseBody
    @ApiOperation(value = "view products b")
    public Product viewProductB() {
        log.info("View product B");
        return new Product("product B");
    }


}
