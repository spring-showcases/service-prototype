package com.gitlab.spring.showcases.service.cxf;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.jaxrs.swagger.Swagger2Feature;
import org.apache.cxf.jaxws.spring.JaxWsWebServicePublisherBeanPostProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.gitlab.spring.showcases.service")
@EntityScan("com.gitlab.spring.showcases.service")
public class CxfServiceApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(CxfServiceApplication.class, args).registerShutdownHook();
    }

    @Bean
    public JacksonJsonProvider jacksonJsonProvider()
    {
        return new JacksonJsonProvider();
    }

    @Bean
    public Swagger2Feature swagger2Feature()
    {
        Swagger2Feature swagger2Feature = new Swagger2Feature();
        swagger2Feature.setTitle("CXF + Swagger");
        return swagger2Feature;
    }

    /**
     * Used to provide scanning of {@link javax.jws.WebService} annotated beans
     * @param bus used as default
     * @return web service publisher
     * @throws NoSuchMethodException
     * @throws ClassNotFoundException
     */
    @Bean
    public JaxWsWebServicePublisherBeanPostProcessor jaxWsWebServicePublisherBeanPostProcessor(Bus bus)
            throws NoSuchMethodException, ClassNotFoundException
    {
        BusFactory.setDefaultBus(bus);
        JaxWsWebServicePublisherBeanPostProcessor result = new JaxWsWebServicePublisherBeanPostProcessor();
        result.setUrlPrefix("/ws");
        return result;
    }

}
