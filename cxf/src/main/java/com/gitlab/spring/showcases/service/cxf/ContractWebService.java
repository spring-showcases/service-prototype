package com.gitlab.spring.showcases.service.cxf;

import com.gitlab.spring.showcases.service.core.contract.Contract;
import com.gitlab.spring.showcases.service.core.contract.ContractOperations;
import com.gitlab.spring.showcases.service.core.contract.ContractNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class ContractWebService
{
    private final ContractOperations contractLogic;

    @Autowired
    public ContractWebService(final ContractOperations contractLogic)
    {
        Assert.notNull(contractLogic);
        this.contractLogic = contractLogic;
    }

    @WebMethod
    public Contract getContractByContractId(@WebParam(name = "contractId") final String contractId)
    {
        try {
            return contractLogic.getContractById(contractId);
        } catch (ContractNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @WebMethod
    public List<Contract> getContractsByCustomerId(@WebParam(name = "customerId") final String customerId)
    {
        return contractLogic.getContractsByCustomerId(customerId);
    }

    @WebMethod
    public Contract createContract(@WebParam(name = "contract") final Contract contract)
    {
        return contractLogic.saveContract(contract);
    }

    @WebMethod
    public Contract updateContract(@WebParam(name = "contractId") final String contractId, @WebParam(name = "customerId") final String customerId)
    {
        final Contract contract;
        try {
            contract = contractLogic.getContractById(contractId);
            contract.setCustomerId(customerId);
            contractLogic.saveContract(contract);
        } catch (ContractNotFoundException e) {
            throw new RuntimeException(e);
        }

        return contract;
    }

    @WebMethod
    public boolean deleteContractById(@WebParam(name = "contractId") final String contractId)
    {
        try {
            return contractLogic.deleteContractById(contractId);
        } catch (ContractNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
