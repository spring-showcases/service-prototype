package com.gitlab.spring.showcases.service.cxf;

import com.gitlab.spring.showcases.service.core.faultexample.FaultDelegate;
import org.apache.cxf.binding.soap.SoapFault;
import org.springframework.stereotype.Component;

@Component
public class SoapFaultDelegate implements FaultDelegate {
    @Override
    public String execute() {
        throw new SoapFault("Executed fault!", null);
    }
}
