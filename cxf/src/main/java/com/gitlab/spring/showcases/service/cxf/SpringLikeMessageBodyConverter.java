package com.gitlab.spring.showcases.service.cxf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.stereotype.Component;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Component
@Provider
public class SpringLikeMessageBodyConverter implements MessageBodyReader, MessageBodyWriter
{

    @Autowired
    private MappingJackson2XmlHttpMessageConverter mappingJackson2XmlHttpMessageConverter;

    @Override
    public boolean isReadable(Class type, Type genericType, Annotation[] annotations, MediaType mediaTypeOrig) {
        org.springframework.http.MediaType mediaType = org.springframework.http.MediaType.valueOf(mediaTypeOrig.toString());
        return mappingJackson2XmlHttpMessageConverter.canRead(type, mediaType);
    }

    @Override
    public Object readFrom(Class type,
                           Type genericType,
                           Annotation[] annotations,
                           MediaType mediaType,
                           MultivaluedMap httpHeaders,
                           InputStream entityStream) throws IOException, WebApplicationException {
        HttpInputMessage inputMessage = new HttpInputMessage() {
            @Override
            public InputStream getBody() throws IOException {
                return entityStream;
            }

            @Override
            public HttpHeaders getHeaders() {
                HttpHeaders result = new HttpHeaders();
                result.putAll(httpHeaders);
                return result;
            }
        };
        return mappingJackson2XmlHttpMessageConverter.read(type, inputMessage);
    }

    @Override
    public boolean isWriteable(Class type, Type genericType, Annotation[] annotations, MediaType mediaTypeOrig) {
        org.springframework.http.MediaType mediaType = org.springframework.http.MediaType.valueOf(mediaTypeOrig.toString());
        return mappingJackson2XmlHttpMessageConverter.canWrite(type, mediaType);
    }

    @Override
    public long getSize(Object o, Class type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return 0;
    }

    @Override
    public void writeTo(Object o,
                        Class type,
                        Type genericType,
                        Annotation[] annotations,
                        MediaType mediaTypeOrig,
                        MultivaluedMap httpHeaders,
                        OutputStream entityStream) throws IOException, WebApplicationException {
        org.springframework.http.MediaType mediaType = org.springframework.http.MediaType.valueOf(mediaTypeOrig.toString());
        HttpOutputMessage outputMessage = new HttpOutputMessage() {
            @Override
            public OutputStream getBody() throws IOException {
                return entityStream;
            }

            @Override
            public HttpHeaders getHeaders() {
                HttpHeaders result = new HttpHeaders();
                result.putAll(httpHeaders);
                return result;
            }
        };
        mappingJackson2XmlHttpMessageConverter.write(o, type, mediaType, outputMessage);
    }
}
