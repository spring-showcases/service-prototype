package com.gitlab.spring.showcases.service.core;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

@Configuration
@EnableNeo4jRepositories
public class ServiceCoreConfiguration {
}
