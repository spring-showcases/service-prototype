package com.gitlab.spring.showcases.service.core.contract;

import java.util.function.Function;

public class ContractNotFoundException extends Throwable {
    private static final Function<String, String> MESSAGE_FUNCTION = contractId -> String.format("Contract with id %s not found", contractId);

    public ContractNotFoundException(String contractId) {
        super(MESSAGE_FUNCTION.apply(contractId));
    }
}
