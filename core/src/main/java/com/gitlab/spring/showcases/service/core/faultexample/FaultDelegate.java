package com.gitlab.spring.showcases.service.core.faultexample;

public interface FaultDelegate {
    String execute();
}
