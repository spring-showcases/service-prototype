package com.gitlab.spring.showcases.service.core.contract;

import lombok.Data;
import org.neo4j.ogm.annotation.NodeEntity;

import javax.xml.bind.annotation.XmlRootElement;

@NodeEntity
@XmlRootElement(name = "contract")
@Data
public class Contract
{
    private Long id;

    private String contractId;
    private String customerId;
}
