package com.gitlab.spring.showcases.service.core.product;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Component
@Path("/")
@Produces({"application/json", "application/xml"})
@Consumes({"application/json", "application/xml"})
@Api(value = "This resource is used to access product A")
@Slf4j
public class ProductResourceA {

    @Path("/showA")
    @GET
    @ApiOperation(value = "show products a")
    public Product showProductA() {
        log.info("Show product A");
        return new Product("product A");
    }
}
