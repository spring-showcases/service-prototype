package com.gitlab.spring.showcases.service.core.product;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Product {
    private String description;

    public Product(String description) {
        this.description = description;
    }

    public Product() {

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
