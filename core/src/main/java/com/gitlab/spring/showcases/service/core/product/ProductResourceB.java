package com.gitlab.spring.showcases.service.core.product;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Component
@Path("/")
@Produces({"application/json", "application/xml"})
@Consumes({"application/json", "application/xml"})
@Api(value = "This resource is used to access product B")
@Slf4j
public class ProductResourceB {

    @Path("/viewB")
    @GET
    @ApiOperation(value = "view products b")
    public Product viewProductB() {
        log.info("View product B");
        return new Product("product B");
    }
}
