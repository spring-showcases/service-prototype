package com.gitlab.spring.showcases.service.core.contract;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.ws.rs.*;
import java.util.List;

@Component
@Path("rest")
@Produces("application/json")
@Consumes("application/json")
@Api(value = "This resource is used to access the contract domain", produces = "application/json", consumes = "application/json")
@Slf4j
public class ContractResource
{
    private final ContractOperations contractOperations;

    @Autowired
    public ContractResource(final ContractOperations contractOperations)
    {
        Assert.notNull(contractOperations);
        this.contractOperations = contractOperations;
    }

    @GET
    @Path("/contracts/{contractId}")
    @ApiOperation(value = "Query a contract by its id", response = Contract.class)
    public Contract getContractByContractId(@PathParam("contractId") final String contractId)
    {
        try {
            return contractOperations.getContractById(contractId);

        } catch (ContractNotFoundException e) {
            log.debug("nothing found", e);
            throw new NotFoundException(e.getMessage());
        }
    }

    @GET
    @Path("/customers/{customerId}/contracts")
    @ApiOperation(value = "Query all contracts of a customer identified by its id", response = List.class)
    public List<Contract> getContractsByCustomerId(@PathParam("customerId") final String customerId)
    {
        return contractOperations.getContractsByCustomerId(customerId);
    }

    @POST
    @Path("/contracts")
    @ApiOperation(value = "Create a new contract", response = Contract.class)
    public Contract createContract(final Contract contract)
    {
        return contractOperations.saveContract(contract);
    }

    @PUT
    @Path("/contracts/{contractId}")
    @ApiOperation(value = "Update a contract by it's id", response = Contract.class)
    public Contract updateContract(@PathParam("contractId") final String contractId, final Contract contract)
    {
        Contract result;
        try {
            result = contractOperations.getContractById(contractId);
            result.setCustomerId(contract.getContractId());
            result = contractOperations.saveContract(result);

        } catch (ContractNotFoundException e) {
            throw new NotFoundException(e);
        }

        return result;
    }

    @DELETE
    @Path("/contracts/{contractId}")
    @ApiOperation(value = "Delete a contract by it's id", response = boolean.class)
    public boolean deleteContractById(@PathParam("contractId") final String contractId)
    {
        try {
            return contractOperations.deleteContractById(contractId);

        } catch (ContractNotFoundException e) {
            throw new NotFoundException(e);
        }
    }
}

