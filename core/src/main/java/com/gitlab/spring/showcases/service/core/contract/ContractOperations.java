package com.gitlab.spring.showcases.service.core.contract;

import java.util.List;

public interface ContractOperations
{
    Contract getContractById(String contractId) throws ContractNotFoundException;

    List<Contract> getContractsByCustomerId(String customerId);

    Contract saveContract(Contract contract);

    Boolean deleteContractById(String contractId) throws ContractNotFoundException;
}
