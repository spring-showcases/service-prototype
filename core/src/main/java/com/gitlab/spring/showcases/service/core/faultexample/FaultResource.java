package com.gitlab.spring.showcases.service.core.faultexample;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Component
@Path("/fault")
@Api(description = "Resource that throws a CXF SoapFault")
public class FaultResource {

    @Autowired
    private FaultDelegate faultDelegate;

    @Path("/execute")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @ApiOperation(value = "operation that throws the soap fault")
    public String execute() {
        return faultDelegate.execute();
    }

}
