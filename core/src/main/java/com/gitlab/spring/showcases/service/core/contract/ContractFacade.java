package com.gitlab.spring.showcases.service.core.contract;

import lombok.extern.slf4j.Slf4j;
import org.fest.util.Lists;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
class ContractFacade implements ContractOperations
{
    private static final List<String> CONTRACT_IDS = Lists.newArrayList("12345678", "546546546", "465465498", "165465465");

    private final ContractRepository contractRepository;

    public ContractFacade(ContractRepository contractRepository) {this.contractRepository = contractRepository;}

    @Override
    public Contract getContractById(final String contractId) throws ContractNotFoundException {
        log.info("Contract for id {} requested", contractId);
        Contract result = contractRepository.findOneByContractId(contractId);
        if(result == null) {
            throw new ContractNotFoundException(contractId);
        }

        return result;
    }

    @Override
    public List<Contract> getContractsByCustomerId(final String customerId)
    {
        log.info("Contracts for customer {} requested", customerId);
        return contractRepository.findByCustomerId(customerId);
    }

    @Override
    public Contract saveContract(final Contract contract)
    {
        log.info("Saving {}", contract);
        return contractRepository.save(contract);
    }

    @Override
    public Boolean deleteContractById(final String contractId) throws ContractNotFoundException {
        log.info("Deleting contract with id {}", contractId);
        Contract contract = contractRepository.findOneByContractId(contractId);
        if(contract == null) {
            throw new ContractNotFoundException(contractId);
        }
        contractRepository.delete(contract);
        return true;
    }
}
