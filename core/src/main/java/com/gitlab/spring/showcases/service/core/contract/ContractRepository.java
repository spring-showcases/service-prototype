package com.gitlab.spring.showcases.service.core.contract;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractRepository extends PagingAndSortingRepository<Contract, Long> {
    Contract findOneByContractId(String contractId);
    List<Contract> findByCustomerId(String customerId);
}
