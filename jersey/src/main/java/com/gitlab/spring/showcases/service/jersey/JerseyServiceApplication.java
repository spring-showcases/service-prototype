package com.gitlab.spring.showcases.service.jersey;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.config.SwaggerContextService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@ComponentScan("com.gitlab.spring.showcases.service")
@EntityScan("com.gitlab.spring.showcases.service")
@EnableSwagger2
public class JerseyServiceApplication  {

    public static void main(String[] args) {
        SpringApplication.run(JerseyServiceApplication.class, args).registerShutdownHook();
    }


    @Bean
    public BeanConfig swaggerConfig() {
        BeanConfig swaggerConfig = new BeanConfig();
        swaggerConfig.setConfigId("showcase");
        swaggerConfig.setTitle("Jersey + Swagger");
        swaggerConfig.setBasePath("/showcase");
        return swaggerConfig;
    }

    @Bean
    public SwaggerContextService swaggerContextService(BeanConfig swaggerConfig) {
        SwaggerContextService swaggerContextService = new SwaggerContextService();
        swaggerContextService.withSwaggerConfig(swaggerConfig);
        swaggerContextService.initConfig();
        return swaggerContextService;
    }

}
