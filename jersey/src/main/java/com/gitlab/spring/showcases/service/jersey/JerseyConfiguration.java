package com.gitlab.spring.showcases.service.jersey;

import com.gitlab.spring.showcases.service.core.contract.ContractResource;
import com.gitlab.spring.showcases.service.core.product.ProductResourceA;
import com.gitlab.spring.showcases.service.core.product.ProductResourceB;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("/showcase")
@Slf4j
public class JerseyConfiguration extends ResourceConfig {

    public JerseyConfiguration() {
        register(ContractResource.class);
        register(ProductResourceA.class);
        register(ProductResourceB.class);

        // Available at localhost:port/swagger.json
        register(SwaggerSerializers.class);
        register(ApiListingResource.class);
    }

}
